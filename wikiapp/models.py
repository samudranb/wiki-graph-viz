from py2neo import Graph, Node, Relationship, authenticate
# from flask import current_app
import logging

authenticate("localhost:7474", "neo4j", "dw7xJjUb$Jn$")
graph = Graph()

# current_app.logger.debug("Graph:")
# current_app.logger.debug(graph)

# current_app.


class Topic:
    def __init__(self, title, url=None):
        self.title = title
        self.url = url
        print "created object: ", title, url

    def find(self):
        topic = graph.find_one("Topic", "title", self.title)
        print topic
        return topic

    def get_outgoing_topics(self, topic):
        query = "match (a:Topic {title:{title}})-[:LINKS_TO]->(b:Topic) " \
                "return b.title as title, b.url as url " \
                "order by b.incoming DESC " \
                "limit 8"
        records = graph.cypher.execute(query, title=topic)
        print "inside get_outgoing_topics"
        returnObject = []
        for record in records:
            print record.title, record.url
            returnObject.append(
                {
                    'title': record.title,
                    'url': record.url
                }
            )
        print returnObject
        return returnObject

    def get_incoming_topics(self, topic):
        # current_topic = self.find()

        query = "match (a:Topic {title:{title}})<-[:LINKS_TO]-(b:Topic) " \
                "return b.title as title, b.url as url " \
                "order by b.incoming DESC " \
                "limit 8"
        records = graph.cypher.execute(query, title=topic)
        print "inside get_incoming_topics"
        returnObject = []
        for record in records:
            print record.title, record.url
            returnObject.append(
                {
                    'title': record.title,
                    'url': record.url
                }
            )
        print returnObject
        return returnObject
