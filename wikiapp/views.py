from models import Topic
from flask import Flask, request, session, redirect, url_for, abort, render_template, flash, json, jsonify

app = Flask(__name__)

@app.route('/')
def wikigraph():
    return render_template('wikiview.html')

@app.route('/get_outgoing/<topic>')
def get_outgoing(topic):
    print "topic: ", topic
    t = Topic(topic)

    topic_list = t.get_outgoing_topics(topic)

    return json.dumps(topic_list)

    # return jsonify(stub_topic_list)
    return json.dumps(stub_topic_list)

@app.route('/get_incoming/<topic>')
def get_incoming(topic):
    print "topic: ", topic
    t = Topic(topic)

    topic_list = t.get_incoming_topics(topic)

    return json.dumps(topic_list)