"""
This creates the wiki graph on a NEO4J graph db
"""
import wikiparser

__author__ = "Samudra Neelam Bhuyan"

from py2neo import Graph, authenticate


def find_unprocessed_node():
    # g = graph.cypher.execute("MATCH (n:Topic {processed:'False'}) RETURN n LIMIT 1")
    g = graph.cypher.execute("MATCH (n:Topic {is_processed:'False'}) RETURN n, n.title as title, n.url as url LIMIT 1")

    if g.one is None:
        return
    else:
        return g.one


def create_relationship(topic, new_topic):
    """
    Creates a relationship between topic and new_topic
    :param topic:
    :param new_topic:
    :return:
    """
    print topic, new_topic, type(topic) == type(new_topic)

    tx = graph.cypher.begin()  # begin transaction

    # if not new_topic.exists():
    # create_new_node(new_topic)
    # create_rel(topic, new_topic)

    create_node_statement = "MERGE (new_topic:Topic {title:{new_topic_title}, url:{new_topic_url}}) " \
                            "ON CREATE SET new_topic.is_processed = 'False', new_topic.incoming = 1 " \
                            "RETURN new_topic"
    tx.append(create_node_statement,
              {"new_topic_title": new_topic[0],
               "new_topic_url": new_topic[1]})

    # tx.commit()
    # tx = graph.cypher.begin()

    create_rel_statement = "MATCH (a:Topic {title:{topic_title}, url:{topic_url}}), " \
                           "(b:Topic {title:{new_topic_title}, url:{new_topic_url}}) " \
                           "MERGE (a)-[r:LINKS_TO]->(b)" \
                           "SET a.is_processed = 'True', b.incoming = b.incoming+1 " \
                           "RETURN r"

    tx.append(create_rel_statement,
               {"topic_title": topic[0],
                "topic_url": topic[1],
                "new_topic_title": new_topic[0],
                "new_topic_url": new_topic[1]})

    # update_topic_statement = "MATCH (a:Topic {title:{}})" \
    #                          "SET a.is_processed = 'True' " \
    #                          "RETURN a"

    tx.commit()


    # statement = "MATCH (a {url:"{topic_url}"}), (b {url:"{new_topic_url}"}) CREATE (a)-[r:LINKS_TO]->(b)"


# >>> tx = graph.cypher.begin()
# >>> statement = "MATCH (a {name:{A}}), (b {name:{B}}) CREATE (a)-[:KNOWS]->(b)"
# >>> for person_a, person_b in [("Alice", "Bob"), ("Bob", "Dave"), ("Alice", "Carol")]:
# ...     tx.append(statement, {"A": person_a, "B": person_b})
# ...
# >>> tx.commit()

# graph.cypher.execute("CREATE (c:Person {name:{N}}) RETURN c", {"N": "Carol"})




def mark_node_processed(topic):

    tx = graph.cypher.begin()
    statement = "MATCH (a:Topic {title:{topic_title}, url:{topic_url}}) " \
                "SET a.is_processed = 'True' " \
                "RETURN a"

    tx.append(statement,
              {"topic_title": topic[0],
               "topic_url": topic[1]})

    tx.commit()

    print topic[0] + " marked as processed."


def process_nodes():
    """
    This function finds the 1st unprocessed node in the graph, processes the links from that node by creating
    new nodes, until there are no more links left to process
    :return: boolean that indicates are all the links for that node processed?
    """
    topic1 = find_unprocessed_node()
    completed = False
    if topic1 is None:
        completed = True
    else:
        # continue to process the new node
        print topic1.title, topic1["url"], type(topic1)
        topic = [topic1.title, topic1.url]
        print topic
        links = wikiparser.get_outbound_links(topic1.url)

        if len(links) == 0:
            mark_node_processed(topic)
            completed = False
            return completed
        else:
            # print links
            for link in links:
                print link[0], link[1]
                create_relationship(topic, link)
            # completed = True # Kill Switch for testing
                completed = False

    return completed

    #
    # while (topic1 != NULL):
    # links = get_outbound_links(topic1)
    #     for link in links:
    #         if relationship_exists(topic1, link):
    #            pass
    #         else:
    #             topic2 = get_new_node(link)
    #             create_new_relationship(topic1, topic2)


if __name__ == "__main__":
    # authenticate and fetch graph object
    authenticate("localhost:7474", "neo4j", "dw7xJjUb$Jn$")
    graph = Graph()

    is_complete = False

    while not is_complete:
        is_complete = process_nodes()

    print "Finished processing graph. No more nodes to process"
    exit(0)


    # #########################

    # graph.delete_all()
    # print "Deleted"

    # t = graph.find("Topic", "url", "http://en.wikipedia.org/wiki/Austrian_State_Treaty")
    # url = "http://en.wikipedia.org/wiki/Philosophy"
