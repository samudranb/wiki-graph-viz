#!/usr/bin/env python

"""wikiparser.py: This extracts the first few paragraphs of a wikipedia page, whose URL is provided.
   The paragraphs are then parsed for the outbound links, which are returned in a list."""

__author__ = 'Dinu Nair, Samudra Neelam Bhuyan'
__version__ = "1.0"

import requests
from bs4 import BeautifulSoup

def get_intro_paras(url):
    """
    This function will take in a wikipedia URL and return the first few paragraphs before the Table of Contents on that page.
    :param url: wikipedia url (not validated)
    :return: list of paragraphs
    """
    page_http_response = requests.get(url, verify=False)
    para_list = []

    if page_http_response.status_code != 200:
        print("Error in getting page from URL.")
        return None

    wiki_file = BeautifulSoup(page_http_response.text)

    toc = wiki_file.find('div', id='toc')

    if toc is not None:
        print "TOC present"
        for siblings in toc.previous_siblings:
            if siblings.name == 'p':
                para_list.append(siblings)

        para_list.reverse()
    #empty para list indicates some issue with TOC, so defaulting to first 3 paras
    if not para_list:
        # //*[@id="mw-content-text"]/p[1]
        print "TOC absent"
        mw_text = wiki_file.body.find_all('p')
        for index, para in enumerate(mw_text):
            if index >=3:
                break
            para_list.append(para)
        # child = mw_text.contents[1]
        # print child
        # para_list.append(child)
    
    return para_list


def extract_links_from_para(para):
    """
    This function will take in a paragraph of HTML and returns a list of all links and the corresponding titles for the links
    :param para: paragraph from which links are to be extracted
    :return: list of lists, each inner list represents a link with structure as [title, url]
    """

    links = []

    for link in para.find_all('a', recursive=False):
        print link

        try:
            relevant_link = link['href'].split('#')[0]
            title = link['title']
            full_link = "http://en.wikipedia.org" + relevant_link

        except Exception, e:
            print e
        else:
            links.append([title, full_link])

    return links

def get_outbound_links(url):
    """
    This function takes in a wikipedia page URL and returns a list of outgoing links
    :param url: url of the page to be parsed
    :return: list of lists of links
    """
    paras = get_intro_paras(url)

    links = []

    if paras is not None:
        for para in paras:
            links += extract_links_from_para(para)

    return links


if __name__ == "__main__":
    print "This is only for demo"
    # This works for Philosophy
    # paras = get_intro_paras("http://en.wikipedia.org/wiki/Philosophy")

    # This does not work
    paras = get_intro_paras("http://en.wikipedia.org/wiki/Life")

    print len(paras)

    links = []

    for para in paras:
        links += extract_links_from_para(para)
        print para

    for url in links:
        print url
